package com.calxcymru.minigameapi.managers;

import com.calxcymru.minigameapi.arena.Arena;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class ScoreboardManager {

    // List of all scoreboard managers and there arenas
    private static Map<String, ScoreboardManager> managers = new ConcurrentHashMap<>();

    // Map to hold
    private Map<String, Integer> values = new ConcurrentHashMap<>();

    // Instance of the scoreboard manger
    private Arena arena;

    // The scoreboard for the arena
    private Scoreboard scoreboard;

    private Scoreboard blankScoreboard;

    // The main objective of the scoreboard
    private Objective objective;

    // Title of the objective
    private String title;

    private ScoreboardManager(Arena arena, String title) {
        setupScoreboard(arena, title);
        blankScoreboard = getArena().getAPI().getPlugin().getServer().getScoreboardManager().getNewScoreboard();
    }

    /**
     * Get the scoreboard manager for an arena
     *
     * @param arena The arena you want to get the scoreboard for
     * @return The ScoreboardManager for that arena
     */
    public static ScoreboardManager getManager(Arena arena) {
        return getManager(arena, arena.getName());
    }

    /**
     * Get the scoreboard manager for an arena
     *
     * @param arena The arena you want to get the scoreboard for
     * @param title The title for the scoreboard if the manager does not already exist.
     * @return The ScoreboardManager for that arena
     */
    public static ScoreboardManager getManager(Arena arena, String title) {
        if (!managers.containsKey(arena.getName())) {
            managers.put(arena.getName(), new ScoreboardManager(arena, format(title)));
        }
        return managers.get(arena.getName());
    }

    /**
     * Setup the scoreboard
     *
     * @param arena The arena that the scoreboard belongs to
     * @param title The title of the scoreboard
     */
    private void setupScoreboard(Arena arena, String title) {
        this.arena = arena;

        setTitle(title);
        getObjective().setDisplayName(getTitle());
        getObjective().setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    /**
     * Get the arena that the scoreboard manager belongs to
     *
     * @return Instance of arena
     */
    public Arena getArena() {
        return this.arena;
    }

    /**
     * Gets the scoreboard that can be displayed players(s)
     *
     * @return Scoreboard object
     */
    public Scoreboard getScoreboard() {
        if (this.scoreboard == null) {
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        return this.scoreboard;
    }

    /**
     * Get the main objective of the scoreboard where all of the data is held
     *
     * @return Objective for the scoreboard
     */
    public Objective getObjective() {
        if (this.objective == null) {
            this.objective = getScoreboard().registerNewObjective(getArena().getName(), "dummy");
        }
        return this.objective;
    }

    /**
     * Gets the title of the scoreboard with minecraft formatting
     *
     * @return Formatted title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Gets the title of the scoreboard without minecraft formatting
     *
     * @return Unformatted title
     */
    public String getRawTitle() {
        return ChatColor.stripColor(getTitle());
    }

    /**
     * Sets the title for the scoreboard
     *
     * @param title The new title for the scoreboard
     * @return Instance of the current ScoreboardManager class
     */
    public ScoreboardManager setTitle(String title) {
        this.title = format(title);
        getObjective().setDisplayName(title);
        return this;
    }

    /**
     * Get map of items on the scoreboard as well as there score
     *
     * @return Map<String,Integer> of values
     */
    public Map<String, Integer> getValues() {
        return values;
    }

    /**
     * Check to see if a value is already added to the scoreboard
     *
     * @param id The value to check
     * @return True: Added, False: Not added
     */
    public boolean containsValue(String id) {
        return getValues().containsKey(id);
    }

    /**
     * Get the value of an existing item on the scoreboard
     *
     * @param id The value to get
     * @return Value that is on the scoreboard
     */
    public int getValue(String id) {
        if (!containsValue(format(id))) {
            return 0;
        }
        return values.get(format(id));
    }

    /**
     * Set a id on the scoreboard with a specific value
     *
     * @param id    The id of the value
     * @param value Value fo the ID
     * @return Instance of the current ScoreboardManager class
     */
    public ScoreboardManager setValue(String id, int value) {
        id = format(id);
        if (containsValue(id)) {
            removeValue(id);
        }
        getObjective().getScore(id).setScore(value);
        values.put(id, value);
        return this;
    }

    /**
     * Remove a value from the scoreboard
     *
     * @param id The value to remove
     * @return Instance of the current ScoreboardManager class
     */
    public ScoreboardManager removeValue(String id) {
        getValues().keySet().stream().filter(sId -> sId.equals(id)).forEach(sId -> {
            scoreboard.resetScores(sId);
            values.remove(sId);
        });
        return this;
    }

    /**
     * Send the scoreboard to all players in the arena
     *
     * @return Instance of the current ScoreboardManager class
     */
    public ScoreboardManager send() {
        getArena().getArenaPlayers().forEach(player -> player.setScoreboard(getScoreboard()));
        return this;
    }

    /**
     * Clear all data from the scoreboard
     *
     * @return Instance of the current ScoreboardManager class
     */
    public ScoreboardManager clear() {
        for (String id : getValues().keySet()) {
            scoreboard.resetScores(id);
            values.remove(id);
        }
        getArena().getArenaPlayers().forEach(player -> player.setScoreboard(blankScoreboard));
        return this;
    }

    /**
     * Formats a string so that it uses minecraft colour codes
     *
     * @param format The string to format
     * @return Formatted string
     */
    public static String format(String format) {
        return ChatColor.translateAlternateColorCodes('&', format);
    }

}
