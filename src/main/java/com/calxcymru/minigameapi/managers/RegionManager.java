package com.calxcymru.minigameapi.managers;

import com.calxcymru.minigameapi.arena.Arena;
import com.calxcymru.minigameapi.utils.GameUtils;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */

/**
 * TODO: Make this class work more efficiently and not read from disk as much!
 */
public class RegionManager {

    private static HashMap<String, RegionManager> managers = new HashMap<>();

    // HashMap of all regions
    private HashMap<String, List<Location>> regions = new HashMap<>();

    // Instance of the arena
    private Arena arena;

    protected RegionManager(Arena arena) {
        this.arena = arena;
        getRegions();
    }

    public static RegionManager getManager(Arena arena) {
        if (!managers.containsKey(arena.getName())) {
            managers.put(arena.getName(), new RegionManager(arena));
        }
        return managers.get(arena.getName());
    }

    /**
     * Add a region to the arena
     *
     * @param id  id of the wall
     * @param min Min position of the region
     * @param max Max position of the region
     */
    public void addRegion(String id, Location min, Location max) {
        if (getRegions(id) != null) {
            regions.remove(id);
        }

        List<Location> walls = new ArrayList<>();
        walls.add(min);
        walls.add(max);

        this.regions.put(id, walls);

        arena.getConfig().set("regions." + id + ".min", GameUtils.locationToString(min));
        arena.getConfig().set("regions." + id + ".max", GameUtils.locationToString(max));
        arena.getConfig().save();
    }

    /**
     * Remove a region from the arena
     *
     * @param id ID of the wall to remove
     */
    public void removeRegion(String id) {
        if (getRegions(id) != null) {
            this.regions.remove(id);
        }
        removeWalls(getRegions(id).get(0), getRegions(id).get(1));
        arena.getConfig().set("regions." + id, null);
        arena.getConfig().save();
    }

    /**
     * Get a list of regions for a certain ID
     *
     * @param id ID of the regions
     * @return Min/Max locations of the regions
     */
    public List<Location> getRegions(String id) {
        if (getRegionIDS() == null || !getRegionIDS().contains(id)) {
            return null;
        }
        if (!regions.containsKey(id)) {
            List<Location> wallLocations = new ArrayList<>();
            wallLocations.add(GameUtils.stringToLocation(arena.getConfig().getString("regions." + id + ".min")));
            wallLocations.add(GameUtils.stringToLocation(arena.getConfig().getString("regions." + id + ".max")));
            regions.put(id, wallLocations);
        }
        return regions.get(id);
    }

    public boolean wallExists(String id) {
        return arena.getConfig().getConfigurationSection("regions." + id) != null;
    }

    /**
     * Get a list of all of the regions ids for an arena
     *
     * @return List of ids
     */
    public List<String> getRegionIDS() {
        if (arena.getConfig().getConfigurationSection("regions") == null) return null;
        List<String> ids = new ArrayList<>();
        ids.addAll(arena.getConfig().getConfigurationSection("regions").getKeys(false));
        return ids;
    }


    /**
     * Get a map of all regions for the arena
     *
     * @return Map containing the id (key) and locations (min/max; value)
     */
    public HashMap<String, List<Location>> getRegions() {
        if (this.regions.isEmpty() && getRegionIDS() != null) {
            getRegionIDS().forEach(id -> {
                if (getRegions(id) != null) {
                    this.regions.put(id, getRegions(id));
                }
            });
        }
        return this.regions;
    }

    public List<List<Location>> getRegionLocations() {
        List<List<Location>> wallLocations = new ArrayList<>();
        getRegionIDS().forEach(id -> {
            if (getRegions(id) != null) {
                wallLocations.add(getRegions(id));
            }
        });
        return wallLocations;
    }

    public boolean isInRegion(Location region) {
        for (List<Location> wallLocations : getRegionLocations()) {
            CuboidSelection selection = new CuboidSelection(region.getWorld(), wallLocations.get(0), wallLocations.get(1));
            if (selection.contains(region)) {
                return true;
            }
        }
        return false;
    }

    public boolean isInRegion(Location region, String ID) {
        Location min = null;
        Location max = null;
        for (Location regionLocation : getRegions(ID)) {
            if (min == null) {
                min = regionLocation;
                continue;
            }
            if (max == null) {
                max = regionLocation;
            }
        }
        if (min == null || max == null) {
            return false;
        }
        CuboidSelection selection = new CuboidSelection(region.getWorld(), min, max);
        return selection.contains(region);
    }

    public void addWall(Material material, Location min, Location max) {
        setBlocks(material, min, max);
    }

    public void removeWalls(Location min, Location max) {
        setBlocks(Material.AIR, min, max);
    }

    public void setBlocks(Material material, Location min, Location max) {
        List<Block> blocks = new ArrayList<>();

        for (int x = min.getBlockX(); x <= max.getBlockX(); x++) {
            for (int y = min.getBlockY(); y <= max.getBlockY(); y++) {
                for (int z = min.getBlockZ(); z <= max.getBlockZ(); z++) {
                    Block block = min.getWorld().getBlockAt(new Location(min.getWorld(), x, y, z));
                    if (block.getType() != material) {
                        blocks.add(block);
                    }
                }
            }
        }
        for (Block block : blocks) {
            block.setType(material);
        }
    }

}
