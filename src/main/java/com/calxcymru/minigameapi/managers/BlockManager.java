package com.calxcymru.minigameapi.managers;

import com.calxcymru.minigameapi.arena.Arena;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class BlockManager {

    private static HashMap<String, BlockManager> managers = new HashMap<>();

    private Map<Location, Map<Material, Byte>> blockLocations = new ConcurrentHashMap<>();

    // The arena that the manager belongs to
    private Arena arena;

    public BlockManager(Arena arena) {
        this.arena = arena;
    }

    public static BlockManager getManager(Arena arena) {
        if (!managers.containsKey(arena.getName())) {
            managers.put(arena.getName(), new BlockManager(arena));
        }
        return managers.get(arena.getName());
    }

    public void addBlock(Block block) {
        if (blockLocations.keySet().contains(block.getLocation())) {
            return;
        }

        blockLocations.put(block.getLocation(), getBlockData(block));
    }

    public void removeBlock(Block block) {
        if (!blockLocations.keySet().contains(block.getLocation())) {
            return;
        }
        setBlockData(block, blockLocations.get(block.getLocation()));
        blockLocations.remove(block.getLocation());
    }

    public void resetBlocks() {
        new BukkitRunnable(){
            public void run() {
                blockLocations.keySet().forEach(location -> removeBlock(location.getBlock()));
            }
        }.runTask(arena.getAPI().getPlugin());// /\/ / /\/ _/ /-\
    }

    private Map<Material, Byte> getBlockData(Block block) {
        Map<Material, Byte> data = new ConcurrentHashMap<>();
        data.put(block.getType(), block.getData());
        return data;
    }

    public void setBlockData(Block block, Map<Material, Byte> data) {
        setBlockData(block, data.keySet().iterator().next(), data.entrySet().iterator().next().getValue());
    }

    public void setBlockData(Block block, Material material, Byte data) {
        block.setType(material);
        block.setData(data);
    }

}
