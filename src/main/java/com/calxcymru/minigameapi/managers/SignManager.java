package com.calxcymru.minigameapi.managers;

import com.calxcymru.minigameapi.arena.Arena;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;

import java.util.logging.Level;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class SignManager {

    private Arena arena;
    private Sign sign = null;
    private String[] lines;

    public SignManager(Arena arena) {
        this.arena = arena;
    }

    /**
     * Get the arena instance
     *
     * @return Arena instance
     */
    public Arena getArena() {
        return arena;
    }

    /**
     * Gets the sign that is being managed
     *
     * @return Managed sign
     */
    public Sign getSign() {
        if (sign == null) {
            if (arena.getLocation("sign") == null) {
                return sign;
            }
            if (!(arena.getLocation("sign").getBlock().getState() instanceof Sign) || arena.getLocation("sign").getBlock().getType() == Material.AIR) {
                arena.getLocation("sign").getBlock().setType(Material.WALL_SIGN);
            }
            sign = (Sign) arena.getLocation("sign").getBlock().getState();
        }
        return sign;
    }

    /**
     * Sets the sign for this manager
     *
     * @param sign Sign to manage
     * @return Instance of manager
     */
    public SignManager setSign(Sign sign) {
        this.sign = sign;
        return this;
    }

    /**
     * Update the text on the sign to display valid information about the arena/minigame
     *
     * @return True: Updated sign, False: Sign stayed the same
     */
    public boolean updateSign() {
        clear();

        if (getSign() == null) {
            getArena().getAPI().getLogger().log(Level.WARNING, "No sign setup for arena: " + getArena().getName());
            return false;
        }

        getSign().setLine(0, Arena.getMinigameDisplayName());
        getSign().setLine(1, arena.getName());
        getSign().setLine(2, ChatColor.DARK_GRAY + "" + arena.getPlayers().size() + "/" + arena.getMaxPlayers());
        getSign().setLine(3, ChatColor.GREEN + String.valueOf((arena.getState() != null) ? arena.getState().getName().toUpperCase() : 0));
        getSign().update();

        lines = getSign().getLines();
        return true;
    }

    /**
     * Clears all text from the sign
     */
    public SignManager clear() {
        if (getSign() == null) {
            return this;
        }

        for (int i = 0; i < getSign().getLines().length; i++) {
            getSign().setLine(i, "");
        }
        getSign().update();
        return this;
    }

    /**
     * Sets the text for the sign
     *
     * @param lineNumber The line number
     * @param text       The text to set on that line
     * @return Instance of the current SignManager class
     */
    public SignManager setLine(int lineNumber, String text) {
        if (getSign() == null) {
            return this;
        }
        lines[lineNumber] = ChatColor.translateAlternateColorCodes('&', text);
        //arena.getConfig().set("");
        return this;
    }

}
