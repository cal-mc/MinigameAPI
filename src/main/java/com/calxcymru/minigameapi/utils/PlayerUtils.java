package com.calxcymru.minigameapi.utils;

import com.calxcymru.minigameapi.arena.Arena;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.UUID;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class PlayerUtils {

    /**
     * Sends a CommandSender a formatted message
     *
     * @param sender  The CommandSender to send the message to
     * @param message The message to be sent to the player
     * @return True so that we can use it in commands
     */
    public static boolean sendMessage(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', (!Arena.getMinigameDisplayName().equalsIgnoreCase("")) ? Arena.getMinigameDisplayName() + "&r " + message : "" + "&a" + message));
        return true;
    }

    /**
     * Get a player by there UUID
     *
     * @param playerUUID The UUID of the player to get
     * @return Player object if player is online, null if not
     */
    public static Player getPlayer(UUID playerUUID) {
        if (Bukkit.getOfflinePlayer(playerUUID).isOnline()) {
            return Bukkit.getPlayer(playerUUID);
        }
        return null;
    }

    public static boolean killPlayer(Player player, Location spawn) {
        player.setVelocity(new Vector(0, 0, 0));
        player.setFallDistance(0);
        player.teleport(spawn);
        player.setHealth(player.getMaxHealth());
        player.setFireTicks(0);
        player.getActivePotionEffects().clear();
        player.setFoodLevel(20);
        player.setSaturation(20);
        return true;
    }

}
