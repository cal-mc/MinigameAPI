package com.calxcymru.minigameapi.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class GameUtils {

    /**
     * Convert a location into a string
     *
     * @param location Location to be turned into a string
     * @return Location in a string
     */
    public static String locationToString(Location location) {
        return location.getWorld().getName() + "," + location.getX() + "," + location.getY() + "," + location.getZ() + "," + location.getYaw() + "," + location.getPitch();
    }

    /**
     * Convert a string into a location
     *
     * @param toLocation The string to be converted into a location
     * @return Location from a string
     */
    public static Location stringToLocation(String toLocation) {
        String[] locationString = toLocation.split(",");
        Location location = new Location(Bukkit.getServer().getWorld(locationString[0]), 0, 0, 0);
        location.setX(Double.parseDouble(locationString[1]));
        location.setY(Double.parseDouble(locationString[2]));
        location.setZ(Double.parseDouble(locationString[3]));
        location.setYaw(Float.parseFloat(locationString[4]));
        location.setPitch(Float.parseFloat(locationString[5]));

        return location;
    }

    /**
     * Check to see if a location is equal to another location
     *
     * @param check   The location to check
     * @param against The location to check against
     * @return True: The same, False: Not the same
     */
    public static boolean checkLocation(Location check, Location against) {
        if (check == null || against == null) {
            return false;
        }
        check = new Location(check.getWorld(), check.getBlockX(), check.getBlockY(), check.getBlockZ());
        against = new Location(against.getWorld(), against.getBlockX(), against.getBlockY(), against.getBlockZ());
        return ((check.getWorld() == against.getWorld()) && check.getBlockX() == against.getBlockX() && check.getBlockY() == against.getBlockY() && check.getBlockZ() == against.getBlockZ());
    }

    /**
     * Is a string an int
     *
     * @param number The string we want to check
     * @return True: Yes, False: No
     */
    public static boolean isInt(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
