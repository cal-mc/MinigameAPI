package com.calxcymru.minigameapi.utils;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Custom config manager for YamlConfiguration files
 * <p/>
 * Credit to Aidan Taylor (http://AidanTaylor.net)
 *
 * @author Aidan Taylor
 * @author Callum [CaLxCyMru] Morris
 */
public class Config extends YamlConfiguration {

    // Instance of the JavaPlugin that is using the custom config
    private JavaPlugin plugin;

    // The configuration file
    private File configFile;

    // The file path that we use to get the configuration file
    private String filePath;

    public Config(String file, JavaPlugin plugin) {
        this.plugin = plugin;
        this.filePath = file.replace("/", File.separator);

        if (!filePath.contains(".yml")) {
            filePath += ".yml";
        }

        saveDefaultConfig();
        reloadConfig();
    }

    /**
     * Reloads the configuration file from disk into memory
     */
    public void reloadConfig() {
        try {
            load(getConfigFile());
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

        if (plugin.getResource(filePath) != null) {
            try (Reader defConfigStream = new InputStreamReader(plugin.getResource(filePath), "UTF8")) {

                YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);

                setDefaults(defConfig);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Saves the default configuration file and copies the plugin resource if one exists
     */
    public void saveDefaultConfig() {
        if (!getConfigFile().exists() && plugin.getResource(filePath) != null) {
            plugin.saveResource(filePath, false);
        }
    }

    /**
     * Saves the current configuration file to disk
     */
    public void save() {
        if (getConfigFile() == null) {
            saveDefaultConfig();
        }
        try {
            save(new File(plugin.getDataFolder(), filePath));
            reloadConfig();
        } catch (Exception e) {
            // Do nothing
        }
    }

    public File getConfigFile() {
        if (configFile == null) {
            configFile = new File(plugin.getDataFolder(), filePath);

            if (!configFile.exists()) {
                try {
                    configFile.getParentFile().mkdirs();
                    configFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return configFile;
    }
}
