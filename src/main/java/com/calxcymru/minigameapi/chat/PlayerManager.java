package com.calxcymru.minigameapi.chat;

/**
 * Taken from https://github.com/OvercastNetwork/Channels/
 */

import org.bukkit.entity.Player;

/**
 * Interface to represent a player manager.
 */
public interface PlayerManager {
    /**
     * Gets the channel the player is a member of.
     *
     * @param player The player.
     * @return The channel the player is a member of.
     */
    public Channel getMembershipChannel(final Player player);

    /**
     * Sets the channel the player is a member of. Removes the player from their old membership channel.
     *
     * @param player            The player.
     * @param membershipChannel The channel the player is a member of.
     */
    public void setMembershipChannel(final Player player, Channel membershipChannel);

    /**
     * Removes the specified {@link org.bukkit.entity.Player} from the store.
     *
     * @param player The {@link org.bukkit.entity.Player} to be removed.
     */
    public void removePlayer(final Player player);
}
