package com.calxcymru.minigameapi.chat;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class ChannelManager {

    public static final String GLOBAL_CHANNEL_PARENT_NODE = "channels.global";
    public static final String GLOBAL_CHANNEL_SEND_NODE = ChannelManager.GLOBAL_CHANNEL_PARENT_NODE + ".send";
    public static final String GLOBAL_CHANNEL_RECEIVE_NODE = ChannelManager.GLOBAL_CHANNEL_PARENT_NODE + ".receive";

    /**
     * The global channel.
     */
    private Channel globalChannel;
    /**
     * The default channel.
     */
    private Channel defaultChannel;
    /**
     * The player manager.
     */
    private PlayerManager playerManager;

}
