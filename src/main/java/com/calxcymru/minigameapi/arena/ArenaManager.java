package com.calxcymru.minigameapi.arena;

import com.calxcymru.minigameapi.MinigameAPI;
import com.calxcymru.minigameapi.utils.Config;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class ArenaManager {

    // Instance of the ArenaManager class
    private static ArenaManager arenaManager = null;

    private static String arenaConfigPath = "arenas" + File.separator;

    // Instance of the logger
    public static Logger logger = null;

    // Instance of the MinigameAPI
    private MinigameAPI api;

    public ArenaManager(MinigameAPI api) {
        this.api = api;

        if (logger == null) {
            logger = api.getLogger();
        }
    }

    /**
     * Get the arena manager
     *
     * @return The instance of the ArenaManager class
     */
    public static ArenaManager getManager() {
        return arenaManager;
    }

    /**
     * Sets the arena manager
     *
     * @param api Instance of the the MinigameAPI class
     * @return The arena manager
     */
    public static ArenaManager setManager(MinigameAPI api) {
        if (arenaManager == null) {
            arenaManager = new ArenaManager(api);
        }
        return arenaManager;
    }

    public static String getArenaConfigPath() {
        return arenaConfigPath;
    }

    /**
     * Gets an instance of the MinigameAPI
     *
     * @return Instance of the MinigameAPI
     */
    public MinigameAPI getAPI() {
        return api;
    }

    /**
     * Gets an arena by its name
     *
     * @param arenaName Name of the arena to get
     * @return The arena for the given name
     */
    public Arena getArena(String arenaName) {
        if (arenaName == null) return null;
        Arena arenaByName = null;
        for (Arena arena : Arena.getArenas()) {
            if (arena.getName().equals(arenaName)) {
                arenaByName = arena;
                break;
            }
        }
        return arenaByName;
    }

    /**
     * Load all of the arenas into memory
     */
    public static void loadArenas(MinigameAPI api) {
        File arenaFolder = new File(api.getDataFolder().getPath() + File.separator + getArenaConfigPath());
        if (!arenaFolder.exists() || !arenaFolder.isDirectory()) {
            return;
        }

        if (arenaFolder.listFiles() == null) {
            return;
        }

        for (File arena : arenaFolder.listFiles()) {
            if (arena == null) {
                continue;
            }
            Arena.addArena(new Arena(arena.getName().replace(".yml", ""), api));
        }
    }

    /**
     * Create an arena
     *
     * @param arenaName Name of the new arena
     * @return True: Arena was created, False: Arena was not created
     */
    public boolean createArena(String arenaName) {
        if (isArena(arenaName)) {
            logger.log(Level.SEVERE, "ERROR: Failed to create arena as it already exists!");
            return false;
        }

        // Create the new arena
        Config arenaConfig = new Config(getArenaConfigPath() + arenaName + ".yml", getAPI().getPlugin());
        arenaConfig.saveDefaultConfig();
        arenaConfig.save();

        Arena.addArena(new Arena(arenaName, getAPI()));

        logger.log(Level.INFO, "Created arena " + arenaName + "!");
        return true;
    }

    /**
     * Remove an arena
     *
     * @param arenaName Name of arena to delete
     * @return True: Arena was removed, False: Arena was not removed
     */
    public boolean removeArena(String arenaName) {
        if (!isArena(arenaName)) {
            logger.log(Level.SEVERE, "ERROR: Failed to remove arena as it does not exist!");
            return false;
        }

        // Remove the arena
        Arena.removeArena(getArena(arenaName));
        File arenaFile = new File(getAPI().getDataFolder().getPath() + File.separator + getArenaConfigPath() + arenaName + ".yml");
        File schemFile = new File(getAPI().getDataFolder().getPath() + File.separator + "schematics" + File.separator + arenaName + ".schematic");
        if (arenaFile.exists()) {
            arenaFile.delete();
        }
        if(schemFile.exists()){
            schemFile.delete();
        }

        logger.log(Level.INFO, "Removed arena [" + arenaName + "]!");
        return true;
    }

    /**
     * Check to see if the arena exists
     *
     * @param arenaName The arena to check
     * @return True: Arena exists, False: Arena does not exists
     */
    public boolean isArena(String arenaName) {
        arenaName += ".yml";
        File arenaFolder = new File(getAPI().getDataFolder().getPath() + File.separator + getArenaConfigPath());
        if (!arenaFolder.exists() || !arenaFolder.isDirectory()) {
            return false;
        }

        if (arenaFolder.listFiles() == null) {
            return false;
        }

        for (File arena : arenaFolder.listFiles()) {
            if (arena == null) {
                continue;
            }
            if (arenaName.equals(arena.getName())) {
                return true;
            }
        }

        return false;
    }

}
