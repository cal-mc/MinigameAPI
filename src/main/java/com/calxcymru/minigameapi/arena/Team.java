package com.calxcymru.minigameapi.arena;

import com.calxcymru.minigameapi.utils.GameUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class Team {

    // List of all of the teams
    private static List<Team> teams = new ArrayList<>();

    // The arena the team belongs to
    private Arena arena = null;

    // Name of the arena that is being used
    private String arenaName = null;

    // The teams name
    private String name = null;

    // Display name for the team
    private String displayName = null;

    // The teams prefix
    private String prefix = null;

    // Spawn point for the team
    private Location spawn = null;

    // Current players in the team
    private List<UUID> players = new ArrayList<>();

    /**
     * Constructors
     */
    public Team(String arenaName, String teamName) {
        this.arena = ArenaManager.getManager().getArena(arenaName);
        this.arenaName = arena.getName();
        this.name = teamName;

        teams.add(this);
    }

    public Team(Arena arena, String teamName) {
        this.arena = arena;
        this.arenaName = arena.getName();
        this.name = teamName;

        teams.add(this);
    }


    /**
     * Gets the name of the team
     *
     * @return Name of the team
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets get the arena that the team is in
     *
     * @return Arena that team belongs to
     */
    public Arena getArena() {
        return this.arena;
    }

    public String getArenaName() {
        return this.arenaName;
    }

    /**
     * Gets all of the teams that have been created
     *
     * @return All of the teams in teams list
     */
    public static List<Team> getTeams() {
        return teams;
    }

    /**
     * Gets all of the players that are in the team
     *
     * @return All players that are in team
     */
    public List<UUID> getPlayers() {
        return this.players;
    }

    /**
     * Get the spawn for the team
     *
     * @return The spawn location for the team
     */
    public Location getSpawn() {
        if (this.spawn == null) {
            if (getArena().getConfig().get("team." + getName() + ".spawn") == null) return this.spawn;
            this.spawn = GameUtils.stringToLocation(getArena().getConfig().getString("team." + getName() + ".spawn"));
        }
        return this.spawn;
    }

    /**
     * Sets the spawn for the team
     *
     * @param location The new spawn location
     * @return Instance of the team class
     */
    public Team setSpawn(Location location) {
        this.spawn = location;
        getArena().getConfig().set("team." + getName() + ".spawn", GameUtils.locationToString(location));
        getArena().getConfig().save();
        return this;
    }

    /**
     * Gets the display name for the team
     *
     * @return Teams display name
     */
    public String getDisplayName() {
        if (this.displayName == null) {
            if (getArena().getConfig().get("team." + getName() + ".displayName") == null) return this.displayName;
            this.displayName = ChatColor.translateAlternateColorCodes('&', getArena().getConfig().getString("team." + getName() + ".displayName"));
        }
        return this.displayName;
    }

    /**
     * Sets the Display name for the team
     *
     * @param name New display name for the team
     * @return Instance of the team class
     */
    public Team setDisplayName(String name) {
        this.displayName = name;
        getArena().getConfig().set("team." + getName() + ".displayName", name);
        getArena().getConfig().save();
        return this;
    }

    /**
     * Gets the prefix for the team
     *
     * @return Teams prefix
     */
    public String getPrefix() {
        if (this.prefix == null) {
            if (getArena().getConfig().get("team." + getName() + ".prefix") == null) return this.displayName;
            this.prefix = ChatColor.translateAlternateColorCodes('&', getArena().getConfig().getString("team." + getName() + ".prefix"));
        }
        return this.prefix;
    }

    /**
     * Sets the Display name for the team
     *
     * @param prefix New prefix for the team
     * @return Instance of the team class
     */
    public Team setPrefix(String prefix) {
        this.prefix = prefix;
        getArena().getConfig().set("team." + getName() + ".prefix", prefix);
        getArena().getConfig().save();
        return this;
    }

    /**
     * Add a player to the team
     *
     * @param player The player to be added to the team
     * @return Did we add the player to the team successfully
     */
    public boolean addPlayer(Player player) {
        return addPlayer(player.getUniqueId());
    }

    /**
     * Add a player using their UUID to the team
     *
     * @param player The player to be added to the team using their UUID
     * @return Did we add the player to the team successfully
     */
    public boolean addPlayer(UUID player) {
        return (!isInTeam(player)) && players.add(player);
    }

    /**
     * Remove a player from the team
     *
     * @param player The player we want to remove from the team
     * @return Did we remove the player from the team successfully
     */
    public boolean removePlayer(Player player) {
        return removePlayer(player.getUniqueId());
    }

    /**
     * Remove the player from the team using their UUID
     *
     * @param player The player we want to remove from the team using their UUID
     * @return Did we remove the player from the team successfully
     */
    public boolean removePlayer(UUID player) {
        return (isInTeam(player)) && players.remove(player);
    }

    /**
     * Gets the team a player is on
     *
     * @param player The player to get the team of
     * @return The players team
     */
    public static Team getPlayerTeam(Player player) {
        return getPlayerTeam(player.getUniqueId());
    }

    /**
     * Gets the team a player is on using their UUID
     *
     * @param player The player to get the team of using their UUID
     * @return The team that the player is on
     */
    public static Team getPlayerTeam(UUID player) {
        Team playerTeam = null;

        for (Team team : teams) {
            if (team.getPlayers().contains(player)) {
                playerTeam = team;
                break;
            }
        }

        return playerTeam;
    }

    /**
     * Check to see if the player is on a team
     *
     * @param player The player we want to check to see if they are in a team
     * @return True if player is in team, false if not.
     */
    public boolean isInTeam(Player player) {
        return isInTeam(player.getUniqueId());
    }

    /**
     * @param player The player we want to check to see if they are in a team using their UUID
     * @return True if player is in team, false if not.
     */
    public static boolean isInTeam(UUID player) {
        return (getPlayerTeam(player) != null);
    }

    /**
     * Find the team with the lowest amount of players
     *
     * @return The team with the least amount of players
     */
    public static Team getSmallestTeam() {
        Team smallestTeam = null;
        for (Team team : getTeams()) {
            if (smallestTeam == null || team.players.size() < smallestTeam.players.size()) {
                smallestTeam = team;
            }
        }

        return smallestTeam;
    }

    /**
     * Find the team with the lowest amount of players
     *
     * @param teams Custom teams collection to loop through
     * @return The team with the least amount of players
     */
    public static Team getSmallestTeam(Collection<Team> teams) {
        Team smallestTeam = null;
        for (Team team : teams) {
            if (smallestTeam == null || team.players.size() < smallestTeam.players.size()) {
                smallestTeam = team;
            }
        }

        return smallestTeam;
    }
}
