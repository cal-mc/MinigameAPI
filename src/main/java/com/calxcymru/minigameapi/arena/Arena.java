package com.calxcymru.minigameapi.arena;

import com.calxcymru.minigameapi.MinigameAPI;
import com.calxcymru.minigameapi.arena.states.State;
import com.calxcymru.minigameapi.managers.BlockManager;
import com.calxcymru.minigameapi.managers.SignManager;
import com.calxcymru.minigameapi.messages.Message;
import com.calxcymru.minigameapi.messages.MessageType;
import com.calxcymru.minigameapi.managers.ScoreboardManager;
import com.calxcymru.minigameapi.utils.Config;
import com.calxcymru.minigameapi.utils.GameUtils;
import com.calxcymru.minigameapi.utils.PlayerUtils;
import com.calxcymru.minigameapi.utils.Schematic;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class Arena {

    // Name of the minigame
    private static String minigameName = null;

    // Display name of the minigame
    private static String minigameDisplayName = null;

    // List of arenas
    private static List<Arena> arenas = new ArrayList<>();

    // Instance of the MinigameAPI class
    private MinigameAPI api;

    // Arena's config file
    private Config arenaConfig = null;

    // List of states for the arenas
    private List<State> states = new LinkedList<>();

    private State state;

    private int stateID;

    // Sign manager for the arena
    private SignManager signManager = null;

    // ScoreboardManager for the arena
    private ScoreboardManager scoreboardManager = null;

    // Block Manager for the arena
    private BlockManager blockManager = null;

    // Selection for the arena
    private CuboidSelection selection = null;

    // Has the arena been started?
    private boolean started = false;

    // Is the arena resetting?
    private boolean resetting = false;

    // Is the arena editable?
    private Boolean editable;

    // Name of the arena
    private String arenaName;

    // Timings
    private HashMap<String, Long> timings = new HashMap<>();

    // Arena teams
    private HashMap<String, Team> teams = new HashMap<>();

    // Locations for various things
    private HashMap<String, Location> locations = new HashMap<>();

    private List<Location> randomSpawns = null;

    // Players
    private List<UUID> players = new ArrayList<>();
    private int playersAmount = 0;
    private Integer minPlayers = null;
    private Integer maxPlayers = null;

    private Random random = new Random();

    /**
     * @param arenaName The name of the arena
     */
    public Arena(String arenaName, MinigameAPI api) {
        this(arenaName, api, null);
    }

    /**
     * @param arenaName The name of the arena
     * @param team      An existing team
     */
    public Arena(String arenaName, MinigameAPI api, Team team) {
        this.api = api;

        this.arenaName = arenaName;

        this.maxPlayers = getMaxPlayers();
        if(team != null) {
            addTeam(team);
        }
        addArena(this);
    }

    /**
     * Adds an arena to the arenas list
     *
     * @param arena The arena to be added to the arena list
     * @return True: The arena was added to the list, False: The arena was not added to the list
     */
    public static boolean addArena(Arena arena) {
        return (!arenas.contains(arena)) && arenas.add(arena);
    }

    public static void setNames(MinigameAPI api) {
        minigameName = (api.getConfig().getString("general.minigameName") != null) ? api.getConfig().getString("general.minigameName") : "Minigame";
        minigameDisplayName = (api.getConfig().getString("general.minigameDisplayName") != null) ? api.getConfig().getString("general.minigameDisplayName") : "&7[&bMinigame&7]";
    }

    /**
     * Removes an arena from the arenas list
     *
     * @param arena The arena to be removed from the arena list
     * @return True: The arena was removed from the list, False: The arena was not removed from the list
     */
    public static boolean removeArena(Arena arena) {
        arena.getSignManager().clear();
        arena.getPlayers().forEach(arena::removePlayer);
        return (arenas.contains(arena)) && arenas.remove(arena);
    }

    /**
     * Gets all of the arenas have been created
     *
     * @return All arenas that are in the arenas list
     */
    public static List<Arena> getArenas() {
        return (arenas != null) ? arenas : null;
    }

    /**
     * Gets the arena that the player is currently in
     *
     * @param player The player that we want to get the arena of
     * @return The arena that the player is currently in
     */
    public static Arena getPlayerArena(Player player) {
        return getPlayerArena(player.getUniqueId());
    }

    /**
     * Gets the arena that the player is currently in by there UUID
     *
     * @param player The UUID of the player that we want to get the arena of
     * @return The arena that the player is currently in
     */
    public static Arena getPlayerArena(UUID player) {
        for (Arena arena : Arena.getArenas()) {
            if (arena.getPlayers().contains(player)) {
                return arena;
            }
        }
        return null;
    }

    /**
     * Get the name of the current minigame
     *
     * @return The minigame name defined in config
     */
    public static String getMinigameName() {
        return (minigameName != null) ? minigameName : "";
    }

    /**
     * Gets the display name for the current minigame
     *
     * @return The displayName of the current minigame
     */
    public static String getMinigameDisplayName() {
        return ChatColor.translateAlternateColorCodes('&', (minigameDisplayName != null) ? minigameDisplayName : "");
    }

    /**
     * Gets an instance of the MinigameAPI
     *
     * @return Instance of the MinigameAPI
     */
    public MinigameAPI getAPI() {
        return api;
    }

    /**
     * Gets the name of the current arena
     *
     * @return Name of the arena
     */
    public String getName() {
        return arenaName;
    }

    /**
     * Get the sign manager for the arena
     *
     * @return The sign manager
     */
    public SignManager getSignManager() {
        if (this.signManager == null) {
            this.signManager = new SignManager(this);
            this.signManager.updateSign();
        }
        return this.signManager;
    }

    /**
     * Set the sign manager for this arena
     *
     * @param signManager New Sign manager
     */
    public Arena setSignManager(SignManager signManager) {
        this.signManager = signManager;
        return this;
    }

    /**
     * Get the scoreboard manager for an arena
     *
     * @return Scoreboard Manager
     */
    public ScoreboardManager getScoreboardManager() {
        if (this.scoreboardManager == null) {
            this.scoreboardManager = ScoreboardManager.getManager(this, getMinigameDisplayName());
        }
        return this.scoreboardManager;
    }

    /**
     * Get the block manager for an arena
     *
     * @return Block Manager
     */
    public BlockManager getBlockManager() {
        if (this.blockManager == null) {
            this.blockManager = BlockManager.getManager(this);
        }
        return this.blockManager;
    }

    /**
     * Get the selection for an arena
     *
     * @return Arena selection
     */
    public CuboidSelection getSelection() {
        if (selection == null) {
            selection = new CuboidSelection(getLocation("min").getWorld(), getLocation("min"), getLocation("max"));
        }
        return this.selection;
    }

    /**
     * Set the selection for the arena
     *
     * @param selection The new selection
     * @return Instance of the current Arena
     */
    public Arena setSelection(CuboidSelection selection) {
        this.selection = selection;
        return this;
    }

    /**
     * Resets the areana back to what it was like when the schematic was saved
     *
     * @return Instance of the current Arena
     */
    public Arena resetArena() {
        resetting = true;
        Schematic.paste(getName(), getLocation("min"), getAPI());
        resetting = false;

        return this;
    }

    /**
     * Has the arena started?
     *
     * @return True: Yes, False: No
     */
    public boolean hasStarted() {
        return started;
    }

    /**
     * Set the arena as started/not started
     *
     * @param started True: Started, False: Not started
     * @return Instance of the current Arena
     */
    public Arena setStarted(boolean started) {
        this.started = started;
        return this;
    }

    /**
     * Stop the arena
     *
     * @return True: Stopped, False: Not stopped
     */
    public boolean stopArena() {
        this.started = false;
        if (!(getState().getName().equals("end"))) {
            new ArrayList<>(getStates()).forEach(state -> {
                if (state.getName().equals("end")) {
                    setState(state);
                }
            });
            return true;
        }
        if(getPlayersAmount() > 0 ) {
            removeAll("&bArena stopped!\nThanks for playing!");
        }
        return true;
    }

    * Get a set of states for the arena
    /**
     *
     * @return Set of states
     */
    public List<State> getStates() {
        return this.states;
    }

    /**
     * Adds a state to the arena
     *
     * @param state State to add
     * @return Instance of the current Arena
     */
    public Arena addState(State state) {
        getAPI().getLogger().log(Level.INFO, "Adding state '" + state.getName() + "' to arena '" + getName() + "'");
        states.add(state);
        if (this.state == null) {
            setState(state);
        }

        return this;
    }

    /**
     * Sets the current state for the arena
     *
     * @param state The state to be set
     * @return Instance of the current Arena
     */
    public Arena setState(State state) {
        if (getState() != null && getState().isStarted()) {
            getState().stop();
        }

        this.state = state;

        if (!states.contains(state)) {
            states.add(state);
        }
        getAPI().getLogger().log(Level.INFO, "Set current state to '" + state.getName() + "' in arena '" + getName() + "'!");
        return this;
    }

    /**
     * Remove a state from the arena
     *
     * @param state State to remove
     * @return Instance of the current Arena
     */
    public Arena removeState(State state) {
        states.remove(state);
        getAPI().getLogger().log(Level.INFO, "Removing state '" + state.getName() + "' from arena '" + getName() + "'");
        return this;
    }

    /**
     * Clear all states for the arena
     *
     * @return Instance of the current Arena
     */
    public Arena clearStates() {
        new ArrayList<>(getStates()).forEach(this::removeState);
        getAPI().getLogger().log(Level.INFO, "All states removed!");
        return this;
    }

    /**
     * Gets the next state
     *
     * @return The next state
     */
    public State getNextState() {
        if(stateID >= getStates().size() - 1) {
            stateID = -1;
        }

        return getStates().get(++stateID);
    }

    public State nextState() {
        if (getState().isStarted()) {
            getState().stop();
        }
        setState(getNextState());
        getState().start();
        return getState();
    }

    /**
     * Gets the state of the arena
     *
     * @return Current stat e
     */
    public State getState() {
        return this.state;
    }

    /**
     * Gets a state from its name
     *
     * @param stateName The name of the state we want to get
     * @return The state with the name we supplied as an argument
     */
    public State getState(String stateName) {
        for (State state : getStates()) {
            if (state.getName().equalsIgnoreCase(stateName)) {
                return state;
            }
        }
        return null;
    }

    /**
     * Get a specific location for the arena
     *
     * @param location The location to get
     * @return The specific location
     */
    public Location getLocation(String location) {
        if (!this.locations.containsKey(location)) {
            if (getConfig().get("locations." + location) == null) return null;
            this.locations.put(location, GameUtils.stringToLocation(getConfig().getString("locations." + location)));
        }
        return this.locations.get(location);
    }

    /**
     * Gets the world that the arena is in
     *
     * @return Arena world
     */
    public World getWorld() {
        return getLocation("min").getWorld();
    }

    /**
     * Check to see if a location ID is already set
     *
     * @param locationID Location ID to check
     * @return True: Location is set, False: Location is not set
     */
    public boolean isLocationSet(String locationID) {
        return locations.containsKey(locationID);
    }

    /**
     * Sets a specific location for the arena
     *
     * @param locationID The id of the specific location
     * @param location   The specific location
     */
    public Arena setLocation(String locationID, Location location) {
        if (isLocationSet(locationID)) {
            this.locations.remove(locationID);
        }
        this.locations.put(locationID, location);
        getConfig().set("locations." + locationID, GameUtils.locationToString(location));
        getConfig().save();
        return this;
    }

    /**
     * Get a specific timing for the arena
     *
     * @param time The time to get
     * @return The specific time
     */
    public long getTime(String time) {
        if (!this.timings.containsKey(time)) {
            if (getConfig().get("timings." + time) == null) return -1;
            this.timings.put(time, getConfig().getLong("timings." + time));
        }
        return this.timings.get(time);
    }

    /**
     * Check to see if a time has been set
     *
     * @param timeID Time to check
     * @return True: Time is set, False: Time is not set
     */
    public boolean isTimeSet(String timeID) {
        return timings.containsKey(timeID);
    }

    /**
     * Sets a specific location for the arena
     *
     * @param locationID The id of the specific location
     * @param time       The specific location
     */
    public Arena setTime(String locationID, long time) {
        if (isTimeSet(locationID)) {
            this.timings.remove(locationID);
        }
        this.timings.put(locationID, time);
        getConfig().set("timings." + locationID, time);
        getConfig().save();
        return this;
    }

    /**
     * Check to see if the arena is in an editable state
     *
     * @return True: Time is set, False: Time is not set
     */
    public Boolean isEditable() {
        if (this.editable == null) {
            if (getConfig().get("editable") == null) return true;
            this.editable = getConfig().getBoolean("editable");
        }
        return this.editable;
    }

    /**
     * Sets the arena to be editable or not
     *
     * @param editable True: Arena can be edited, False: Arena can't be edited
     */
    public Arena setEditable(boolean editable) {
        this.editable = editable;
        getConfig().set("editable", editable);
        getConfig().save();
        return this;
    }

    /**
     * Is the arena currently resetting?
     *
     * @return True: Yes, False: No
     */
    public boolean isResetting() {
        return resetting;
    }

    /**
     * Sets if the arena is resetting or not
     *
     * @param resetting True: Resetting, False: Not resetting
     */
    public void setResetting(boolean resetting) {
        this.resetting = resetting;
    }

    /**
     * Check to see if the arena has a team
     *
     * @param team Team to be queried
     * @return True: The team is in the arena, False: The team is not in the arena
     */
    public boolean isTeam(Team team) {
        return teams.containsValue(team);
    }

    /**
     * Check to see if the arena has a team
     *
     * @param team Team to be queried
     * @return True: The team is in the arena, False: The team is not in the arena
     */
    public boolean isTeam(String team) {
        return teams.containsKey(team);
    }

    /**
     * Adds a team to the arena
     *
     * @param team Team to be added to the arena
     */
    public void addTeam(Team team) {
        teams.put(team.getName(), team);
    }

    public void removeTeam(Team team) {
        teams.remove(team.getName());
    }

    /**
     * Gets the teams that are in the arena
     *
     * @return Teams that belong to the arena
     */
    public Collection<Team> getTeams() {
        if (teams.values().isEmpty()) {
            if (getConfig().get("team") == null) {
                return null;
            }
            getConfig().getConfigurationSection("team").getKeys(false).forEach(this::getTeam);
        }
        return teams.values();
    }

    /**
     * Get a team for the arena
     *
     * @param teamName Name for the team
     * @return The team
     */
    public Team getTeam(String teamName) {
        if (!isTeam(teamName)) {
            Team team = new Team(getName(), teamName);
            addTeam(team);
        }
        return teams.get(teamName);
    }

    /**
     * Check to see if all arena players are in teams
     *
     * @return Instance of the current Arena
     */
    public Arena checkTeams() {
        getArenaPlayers().forEach(player -> {
            if (!Team.isInTeam(player.getUniqueId())) {
                Team.getSmallestTeam(getTeams()).addPlayer(player);
                PlayerUtils.sendMessage(player, "&6You are now " + Team.getPlayerTeam(player).getDisplayName());
            }
        });
        return this;
    }

    /**
     * Get the maximum amount of players for the arena
     *
     * @return Max players
     */
    public int getMaxPlayers() {
        if (this.maxPlayers == null) {
            if (getConfig().get("players.max") == null) {
                this.maxPlayers = 0;
                return this.maxPlayers;
            }
            this.maxPlayers = getConfig().getInt("players.max");
        }
        return this.maxPlayers;
    }

    /**
     * Set the maximum amount of players for the arena
     *
     * @param maxPlayers New Max players
     * @return Instance of the current Arena
     */
    public Arena setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
        getConfig().set("players.max", this.maxPlayers);
        getConfig().save();
        getSignManager().updateSign();
        return this;
    }

    /**
     * Get the minimum amount of players for the arena
     *
     * @return Minimum players
     */
    public int getMinPlayers() {
        if (this.minPlayers == null) {
            if (getConfig().get("players.min") == null) {
                this.minPlayers = 1;
                return this.minPlayers;
            }
            this.minPlayers = getConfig().getInt("players.min");
        }
        return this.minPlayers;
    }

    /**
     * Set the maximum amount of players for the arena
     *
     * @param minPlayers New Max players
     * @return Instance of the current Arena
     */
    public Arena setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
        getConfig().set("players.min", this.minPlayers);
        getConfig().save();
        getSignManager().updateSign();
        return this;
    }

    /**
     * Gets the players that are in the area
     *
     * @return List of players
     */
    public List<UUID> getPlayers() {
        return players;
    }

    public List<Player> getArenaPlayers() {
        ArrayList<Player> players = new ArrayList<>();
        for (UUID playerUUID : getPlayers()) {
            if (!Bukkit.getOfflinePlayer(playerUUID).isOnline()) {
                continue;
            }
            players.add(Bukkit.getPlayer(playerUUID));
        }
        return players;
    }

    /**
     * Gets the amount of players in an arena
     *
     * @return Player amount
     */
    public int getPlayersAmount() {
        return this.playersAmount;
    }

    /**
     * z
     * Is that player playing in this arena?
     *
     * @param player The player that we are checking
     * @return True: Player is in an arena, False: Player is not in an arena
     */
    public static boolean isPlaying(Player player) {
        return isPlaying(player.getUniqueId());
    }

    /**
     * Is that player playing in this arena?
     *
     * @param player The player that we are checking
     * @return True: Player is in an arena, False: Player is not in an arena
     */
    public static boolean isPlaying(UUID player) {
        for (Arena arena : getArenas()) {
            if (arena.getPlayers().contains(player)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add a player to the arena
     *
     * @param player Player to add to the arena
     * @return True: Added, False: Didn't add
     */
    public boolean addPlayer(UUID player) {
        return addPlayer(Bukkit.getPlayer(player));
    }

    /**
     * Add a player to the arena
     *
     * @param player Player to add to the arena
     * @return True: Added, False: Didn't add
     */
    public boolean addPlayer(Player player) {
        if (isPlaying(player)) {
            return !PlayerUtils.sendMessage(player, "&cYou are already in an arena!");
        }
        if (getPlayersAmount() == getMaxPlayers()) {
            return !PlayerUtils.sendMessage(player, "&cThat arena is full!");
        }
        if(isResetting()) {
            return !PlayerUtils.sendMessage(player, "&cThat arena is resetting!");
        }
        playersAmount = playersAmount + 1;

        players.add(player.getUniqueId());
        player.teleport(getLocation("lobby"));
        player.getInventory().clear();
        player.updateInventory();
        sendAll("&6" + player.getName() + " &ahas joined the game! &8(" + getPlayersAmount() + "&8/" + getMaxPlayers() + "&8)");
        getSignManager().updateSign();

        if (getPlayersAmount() == 1) {
            if (getState().getName().equalsIgnoreCase("lobby")) {
                getState().start();
                return true;
            }
            for (State state : getStates()) {
                if (state.getName().equalsIgnoreCase("lobby")) {
                    state.start();
                    return true;
                }
            }
        }

        return true;
    }

    /**
     * Remove a player to the arena
     *
     * @param player Player to remove from the arena
     * @return True: Removed, False: Didn't remove
     */
    public boolean removePlayer(UUID player) {
        return removePlayer(Bukkit.getPlayer(player));
    }

    /**
     * Remove a player from the arena
     *
     * @param player Player to remove from the arena
     * @return True: Removed, False: Didn't remove
     */
    public boolean removePlayer(Player player) {
        return removePlayer(player, "&aYou left the game!");
    }

    public boolean removePlayer(Player player, String reason) {
        if (!isPlaying(player)) {
            return !PlayerUtils.sendMessage(player, "&cYou are not in an arena!");
        }
        playersAmount -= 1;
        if (getPlayersAmount() == 1) {
            stopArena();
        }
        players.remove(player.getUniqueId());
        getSignManager().updateSign();
        player.teleport(getLocation("fallback"));
        player.getInventory().clear();
        sendAll("&6" + player.getName() + " &ahas left the game! &8(" + getPlayersAmount() + "&8/" + getMaxPlayers() + "&8)");
        if (reason != null) {
            PlayerUtils.sendMessage(player, reason);
        }
        return true;
    }

    public boolean removeAll(String reason) {
        getArenaPlayers().forEach(player -> removePlayer(player, reason));
        return getPlayersAmount() == 0;
    }

    /**
     * Send a message to everyone who is in the arena
     *
     * @param message Message to be sent to everyone
     * @return Instance of the current Arena
     */
    public Arena sendAll(String message) {
        return sendAll(message, MessageType.CHAT);
    }

    /**
     * Sends a message to everyone who is the arena in multiple ways
     *
     * @param message     Message to be sent to everyone
     * @param messageType The type of message we want to send the message as
     * @return Instance of the current Arena
     */
    public Arena sendAll(String message, MessageType messageType) {
        Message playerMessage = new Message(message);

        getArenaPlayers().forEach(player -> playerMessage.send(player, messageType));
        return this;
    }

    /**
     * Teleport all players to a specific location
     *
     * @param location The location to teleport everyone to
     * @return Instance of the current Arena
     */
    public Arena teleportAll(Location location) {
        getPlayers().forEach(playerUUID -> Bukkit.getPlayer(playerUUID).teleport(location));
        return this;
    }

    /**
     * Teleport all players, respecting teams to their relevant spawn points
     *
     * @return Instance of the current Arena
     */
    public Arena teleportAllTeam() {
        getArenaPlayers().forEach(player -> player.teleport(Team.getPlayerTeam(player).getSpawn()));
        return this;
    }

    /**
     * Update a sign to display the arena statistics
     *
     * @return Instance of the current Arena
     */
    public Arena updateSign() {
        getSignManager().updateSign();
        return this;
    }

    /**
     * Gets the arena configuration file as yaml
     *
     * @return Arena config file
     */
    public Config getConfig() {
        if (arenaConfig == null) {
            arenaConfig = new Config(ArenaManager.getArenaConfigPath() + getName(), getAPI().getPlugin());
        }
        return arenaConfig;
    }

    public void addRandomSpawn(Location location) {
        if(randomSpawns == null) {
            randomSpawns = new ArrayList<>();
        }
        randomSpawns.add(location);
    }

    public void saveRandomSpawns() {
        if(getRandomSpawns().isEmpty()) {
            return;
        }
        List<String> locations = getRandomSpawns().stream().map(GameUtils::locationToString).collect(Collectors.toList());

        getConfig().set("locations.spawns", locations);
        getConfig().save();
    }

    public List<Location> getRandomSpawns() {
        if(randomSpawns == null) {
            randomSpawns = new ArrayList<>();
            if (getConfig().get("locations.spawns") != null) {
                randomSpawns.addAll(getConfig().getStringList("locations.spawns").stream().map(GameUtils::stringToLocation).collect(Collectors.toList()));
            }
        }
        return randomSpawns;
    }

    public void teleportAllRandom() {
        if(getRandomSpawns().size() <= 1) {
            teleportAll(getLocation("defaultSpawn"));
        }

        getArenaPlayers().forEach(player -> player.teleport(getRandomSpawns().get((getRandomSpawns().size() == 1) ? 0 : random.nextInt(getRandomSpawns().size()))));
    }
}
