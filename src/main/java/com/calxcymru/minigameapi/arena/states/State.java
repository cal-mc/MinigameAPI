package com.calxcymru.minigameapi.arena.states;

import com.calxcymru.minigameapi.arena.Arena;
import org.bukkit.Bukkit;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class State implements Runnable {

    // Arena that the state belongs to
    private Arena arena;

    // Instance of the state
    private State state;

    // Name of the state
    private String stateName;

    // Is the state started?
    private boolean started = false;

    // Keep track of how long the state has ran for
    private long counter = 0;

    private long startCount = 0;

    // The task id for the state
    private int taskID;

    // Have we force ended the state?
    private boolean forceEnd = true;

    public State(Arena arena, String stateName) {
        this.arena = arena;
        this.state = this;
        this.stateName = stateName;
        this.counter = arena.getTime(getName());
        startCount = counter;
    }

    @Override
    public void run() {
        if (!isStarted()) {
            getState().stop();
            return;
        }
    }

    public void setCount(long count) {
        this.counter = count;
    }

    public long getCount() {
        return this.counter;
    }

    /**
     * Start the timer for the state
     */
    public void start() {
        if (!getArena().getState().getName().equalsIgnoreCase(getName())) {
            getArena().setState(getState());
        }

        getArena().getSignManager().updateSign();

        setCount(getArena().getTime(getName()));
        getArena().getScoreboardManager().clear();

        if (!started) {
            taskID = Bukkit.getScheduler().runTaskTimerAsynchronously(getArena().getAPI().getPlugin(), this, 0L, 20L).getTaskId();
        }

        setStarted(true);
    }

    /**
     * Stop the timer for the state
     */
    public void stop() {
        if (isStarted()) {
            setStarted(false);
            Bukkit.getScheduler().cancelTask(getTaskID());
            counter = startCount;
            getArena().getScoreboardManager().clear();
            setCount(arena.getTime(getName()));
            getArena().nextState();
        }
    }

    /**
     * This is the code that we use to end the state correctly
     */
    public void endState(boolean forceEnd) {
        this.forceEnd = forceEnd;
    }

    /**
     * Set the state to started/not started
     *
     * @param started True: Started, False: Not Started
     */
    public void setStarted(boolean started) {
        this.started = started;
    }

    /**
     * Get if the arena has started or not
     *
     * @return True: Started, False: Not started
     */
    public boolean isStarted() {
        return this.started;
    }

    /**
     * Get the arena that the state belongs to
     *
     * @return Arena of state
     */
    public Arena getArena() {
        return this.arena;
    }

    /**
     * Gets the name of the state
     *
     * @return The name of the state
     */
    public String getName() {
        return this.stateName;
    }

    /**
     * Gets the state
     *
     * @return Instance of the state
     */
    public State getState() {
        return this.state;
    }

    public int getTaskID() {
        return this.taskID;
    }

    public boolean hasForceEnded() {
        return forceEnd;
    }

    public void setForceEnd(boolean forceEnd) {
        this.forceEnd = forceEnd;
    }
}
