package com.calxcymru.minigameapi.messages;

/**
 * Crafted in heart of Wales!
 *
 * @author CaLxCyMru
 */
public enum MessageType {
    CHAT, HOTBAR, TITLE, SUBTITLE
}
