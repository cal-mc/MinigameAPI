package com.calxcymru.minigameapi.messages;

import com.calxcymru.minigameapi.MinigameAPI;
import com.calxcymru.minigameapi.utils.Config;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class MessageList {

    /**
     * MESSAGE CONFIG
     */
    private static Config messageConfig = null;

    /**
     * COMMANDS MESSAGES
     */
    public static String COMMAND_INVALID_SENDER;
    public static String COMMAND_INVALID_SYNTAX;
    public static String COMMAND_NO_PERMISSION;

    /**
     * PLAYER MESSAGES
     */
    public static String PLAYER_NOT_ONLINE;
    public static String PLAYER_NOT_FOUND;

    /**
     * TELEPORT MESSAGES
     */
    public static String TELEPORT_TOGGLE;

    /**
     * REQUEST MESSAGES
     */
    public static String REQUEST_SELF;
    public static String REQUEST_NOT_FOUND;
    public static String REQUEST_NOT_FOUND_ANY;
    public static String REQUEST_COOLDOWN;
    public static String REQUEST_TIMEOUT;
    public static String REQUEST_PENDING;
    public static String REQUEST_SENT;
    public static String REQUEST_RECEIVED_TO;
    public static String REQUEST_RECEIVED_HERE;
    public static String REQUEST_ACCEPT_SENDER_TO;
    public static String REQUEST_ACCEPT_RECEIVER_TO;
    public static String REQUEST_ACCEPT_SENDER_HERE;
    public static String REQUEST_ACCEPT_RECEIVER_HERE;
    public static String REQUEST_DENY_SENDER;
    public static String REQUEST_DENY_RECEIVER;

    /**
     * Gets the message form config
     *
     * @param path The path of the message
     * @return The message
     */
    private static String getMessage(String path, MinigameAPI api) {
        return (getMessageConfig(api).getString("messages." + path) != null) ? getMessageConfig(api).getString("messages." + path) : "&cConfig path not found at:\n&6" + path;
    }

    /**
     * Load messages
     */
    public static void loadMessages(MinigameAPI api) {
        /**
         * COMMANDS MESSAGES
         */
        COMMAND_INVALID_SENDER = getMessage("commands.invalid-sender", api);
        COMMAND_INVALID_SYNTAX = getMessage("commands.invalid-syntax", api);
        COMMAND_NO_PERMISSION = getMessage("commands.no-permission", api);

        /**
         * PLAYER MESSAGES
         */
        PLAYER_NOT_ONLINE = getMessage("player.not-online", api);
        PLAYER_NOT_FOUND = getMessage("player.not-found", api);

        /**
         * TELEPORT MESSAGES
         */
        TELEPORT_TOGGLE = getMessage("teleport.toggle", api);

        /**
         * REQUEST MESSAGES
         */
        REQUEST_SELF = getMessage("request.self", api);
        REQUEST_NOT_FOUND = getMessage("request.not-found", api);
        REQUEST_NOT_FOUND_ANY = getMessage("request.not-found-any", api);
        REQUEST_COOLDOWN = getMessage("request.cooldown", api);
        REQUEST_TIMEOUT = getMessage("request.timeout", api);
        REQUEST_PENDING = getMessage("request.pending", api);
        REQUEST_SENT = getMessage("request.sent", api);
        REQUEST_RECEIVED_TO = getMessage("request.received-to", api);
        REQUEST_RECEIVED_HERE = getMessage("request.received-here", api);
        REQUEST_ACCEPT_SENDER_TO = getMessage("request.accept-sender-to", api);
        REQUEST_ACCEPT_RECEIVER_TO = getMessage("request.accept-receiver-to", api);
        REQUEST_ACCEPT_SENDER_HERE = getMessage("request.accept-sender-here", api);
        REQUEST_ACCEPT_RECEIVER_HERE = getMessage("request.accept-receiver-here", api);
        REQUEST_DENY_SENDER = getMessage("request.deny-sender", api);
        REQUEST_DENY_RECEIVER = getMessage("request.deny-receiver", api);
    }

    public static Config getMessageConfig(MinigameAPI api) {
        if (messageConfig == null) {
            messageConfig = new Config("messages.yml", api.getPlugin());
        }
        return messageConfig;
    }
}
