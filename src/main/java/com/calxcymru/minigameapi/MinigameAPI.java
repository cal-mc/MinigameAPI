package com.calxcymru.minigameapi;

import com.calxcymru.minigameapi.arena.Arena;
import com.calxcymru.minigameapi.arena.ArenaManager;
import com.calxcymru.minigameapi.command.CommandManager;
import com.calxcymru.minigameapi.command.commands.MinigameCommand;
import com.calxcymru.minigameapi.listeners.BlockListener;
import com.calxcymru.minigameapi.listeners.PlayerListener;
import com.calxcymru.minigameapi.listeners.SignListener;
import com.calxcymru.minigameapi.menu.InventoryMenuEventListener;
import com.calxcymru.minigameapi.utils.PlayerUtils;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import net.milkbowl.vault.permission.Permission;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class MinigameAPI {

    // Instance of world edit, we only want one
    private static WorldEditPlugin worldEdit = null;

    private Economy economy = null;

    private Permission permissions = null;

    private Chat chat = null;

    private JavaPlugin plugin;

    // PluginManager instance
    private PluginManager manager = Bukkit.getPluginManager();

    // Command manager
    private CommandManager commandManager;


    public MinigameAPI(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Setup the default requirements for the minigame
     */
    public void setupDefaults() {
        ArenaManager.setManager(this);
        ArenaManager.loadArenas(this);
        Arena.setNames(this);

        // Make sure that the command manager isn't null
        commandManager = new CommandManager(getPlugin().getLogger());

        registerListeners();
        registerCommands();

        // Load configs
        getPlugin().getConfig().options().copyDefaults(true);
        getPlugin().saveDefaultConfig();


        if (!new File(getDataFolder() + File.separator + "README.txt").exists() && getPlugin().getResource("README.txt") != null) {
            getPlugin().saveResource("README.txt", true);
        }

        // Update the signs for all arenas!
        Arena.getArenas().forEach(arena -> arena.getSignManager().updateSign());
    }

    /**
     * Use this onDisable, used to unload everything that MinigameAPI uses.
     */
    public void unloadAll() {
        Arena.getArenas().forEach(arena -> {
            new ArrayList<>(arena.getPlayers()).forEach(player -> {
                if (player != null && PlayerUtils.getPlayer(player) != null) {
                    try {
                        PlayerUtils.getPlayer(player).setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
                    } catch (Exception e) {
                        // Do nothing
                    }
                }
                arena.removePlayer(player);
            });
            arena.getBlockManager().resetBlocks();
            arena.getState().stop();
            arena.clearStates();
        });
        Bukkit.getScheduler().cancelTasks(getPlugin());
    }

    /**
     * Register listeners
     */
    public void registerListeners() {
        getManager().registerEvents(new SignListener(), getPlugin());
        getManager().registerEvents(new PlayerListener(this), getPlugin());
        getManager().registerEvents(new InventoryMenuEventListener(), getPlugin());
        getManager().registerEvents(new BlockListener(this), getPlugin());
    }

    public void registerListener(Listener listener) {
        getManager().registerEvents(listener, getPlugin());
    }

    /**
     * Register commands
     */
    public void registerCommands() {
        if (commandManager != null) {
            commandManager.registerCommands(new MinigameCommand(this));
        } else {
            getPlugin().getLogger().warning("Failed to register commands!");
        }
    }

    /**
     * Get the world edit plugin
     *
     * @return World Edit
     */
    public WorldEditPlugin getWorldEdit() {
        if (worldEdit == null) {
            worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
        }
        return worldEdit;
    }

    /**
     * Gets an instance of the plugin manager
     *
     * @return Instance of plugin manager
     */
    public PluginManager getManager() {
        if (manager == null) {
            manager = Bukkit.getPluginManager();
        }
        return manager;
    }

    /**
     * Gets the data folder of the MinigameAPI
     *
     * @return The data folder
     */
    public File getDataFolder() {
        return getPlugin().getDataFolder();
    }

    /**
     * Gets the config file for the MinigameAPI
     *
     * @return The config file
     */
    public FileConfiguration getConfig() {
        return getPlugin().getConfig();
    }

    /**
     * Gets the logger for the MinigameAPI
     *
     * @return Instance of the logger
     */
    public Logger getLogger() {
        return getPlugin().getLogger();
    }

    /**
     * Returns the command manager for the MinigameAPI
     *
     * @return Instance of the command manager
     */
    public CommandManager getCommandManager() {
        return commandManager;
    }

    /**
     * Gets the plugin that the MinigameAPI belongs to
     *
     * @return Instance of the plugin
     */
    public JavaPlugin getPlugin() {
        return plugin;
    }

    /**
     * Gets the vault economy manager
     *
     * @return Instance of vault economy manager
     */
    public Economy getEconomy() {
        if(economy == null){
            if (getPlugin().getServer().getPluginManager().getPlugin("Vault") == null) {
                return null;
            }
            RegisteredServiceProvider<Economy> rsp = getPlugin().getServer().getServicesManager().getRegistration(Economy.class);
            if(rsp != null) {
                economy = rsp.getProvider();
            }
        }
        return economy;
    }

    /**
     * Gets the vault permission manager
     *
     * @return Instance of vault permission manager
     */
    public Permission getPermissions() {
        if(permissions == null){
            if (getPlugin().getServer().getPluginManager().getPlugin("Vault") == null) {
                return null;
            }
            RegisteredServiceProvider<Permission> rsp = getPlugin().getServer().getServicesManager().getRegistration(Permission.class);
            if(rsp != null) {
                permissions = rsp.getProvider();
            }
        }
        return permissions;
    }

    /**
     * Gets the vault chat manager
     *
     * @return Instance of Vault Chat manager
     */
    public Chat getChat() {
        if(chat == null){
            if (getPlugin().getServer().getPluginManager().getPlugin("Vault") == null) {
                return null;
            }
            RegisteredServiceProvider<Chat> rsp = getPlugin().getServer().getServicesManager().getRegistration(Chat.class);
            if(rsp != null) {
                chat = rsp.getProvider();
            }
        }
        return chat;
    }

}
