package com.calxcymru.minigameapi.listeners;

import com.calxcymru.minigameapi.arena.Arena;
import com.calxcymru.minigameapi.utils.GameUtils;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class SignListener implements Listener {

    @EventHandler
    public void onSignRightClick(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK || event.getClickedBlock() == null || !(event.getClickedBlock().getState() instanceof Sign)) {
            return;
        }

        for (Arena arena : Arena.getArenas()) {
            if (GameUtils.checkLocation(event.getClickedBlock().getLocation(), arena.getLocation("sign"))) {
                arena.addPlayer(event.getPlayer());
                return;
            }
        }   
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onSignBreak(BlockBreakEvent event) {
        Arena.getArenas().forEach(arena -> {
            if (GameUtils.checkLocation(event.getBlock().getLocation(), arena.getLocation("sign"))) {
                event.setCancelled(true);
            }
        });
    }

}
