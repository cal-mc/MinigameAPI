package com.calxcymru.minigameapi.listeners;

import com.calxcymru.minigameapi.MinigameAPI;
import com.calxcymru.minigameapi.arena.Arena;
import com.calxcymru.minigameapi.utils.PlayerUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class PlayerListener implements Listener {

    // Instance of the API
    private MinigameAPI api;

    public PlayerListener(MinigameAPI api) {
        this.api = api;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        for (Arena arena : Arena.getArenas()) {
            arena.removePlayer(player);
        }
    }

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event) {
        Player player = event.getPlayer();
        for (Arena arena : Arena.getArenas()) {
            arena.removePlayer(player);
        }
    }

    @EventHandler
    public void onPlayerSendCommand(PlayerCommandPreprocessEvent event) {
        if (Arena.getPlayerArena(event.getPlayer()) == null) {
            return;
        }
        if (api.getPermissions().has(event.getPlayer(), "minigame.command.override")) {
            return;
        }
        if (Arena.getPlayerArena(event.getPlayer()).getAPI().getConfig().get("game.allowed-commands") == null || Arena.getPlayerArena(event.getPlayer()).getAPI().getConfig().getStringList("game.allowed-commands") == null) {
            event.setCancelled(true);
            PlayerUtils.sendMessage(event.getPlayer(), "&cYou cannot perform this command while in game!");
            return;
        }
        String command = event.getMessage();
        List<String> allowedCommands = Arena.getPlayerArena(event.getPlayer()).getAPI().getConfig().getStringList("game.allowed-commands");

        for (String safeCommand : allowedCommands) {
            if(!safeCommand.startsWith("/")){
                safeCommand = "/" +safeCommand;
            }
            if (command.startsWith(safeCommand)) {
                event.setCancelled(false);
                return;
            }
        }

        event.setCancelled(true);
        PlayerUtils.sendMessage(event.getPlayer(), "&cYou cannot perform this command while in game!");
    }

}
