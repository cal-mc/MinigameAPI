package com.calxcymru.minigameapi.listeners;

import com.calxcymru.minigameapi.arena.Arena;
import com.calxcymru.minigameapi.arena.Team;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class ChatListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onChatEvent(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        for (Arena arena : Arena.getArenas()) {
            if (arena.getTeams().isEmpty() || !arena.getPlayers().contains(player.getUniqueId())) continue;
            for (Team team : arena.getTeams()) {
                // TODO: I want to add in chat channels. Do it at some point Callum
            }
        }
    }

}
