package com.calxcymru.minigameapi.listeners;

import com.calxcymru.minigameapi.MinigameAPI;
import com.calxcymru.minigameapi.arena.Arena;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class BlockListener implements Listener {

    // Instance of the API
    private MinigameAPI api;

    public BlockListener(MinigameAPI api) {
        this.api = api;
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!api.getPermissions().has(event.getPlayer(), "minigame.place")) {
            event.setCancelled(true);
        }
        Arena.getArenas().forEach(arena -> {
            if(arena.getArenaPlayers().contains(event.getPlayer())) {
                arena.getBlockManager().addBlock(event.getBlock());
            }
        });
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (!api.getPermissions().has(event.getPlayer(), "minigame.place")) {
            event.setCancelled(true);
            return;
        }

        Arena.getArenas().forEach(arena -> {
            if (event.getBlock().getLocation() == arena.getLocation("sign")) {
                event.setCancelled(true);
                return;
            }
            if(arena.getArenaPlayers().contains(event.getPlayer())) {
                arena.getBlockManager().addBlock(event.getBlock());
            }
        });
    }

}
