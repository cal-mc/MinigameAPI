package com.calxcymru.minigameapi.command.commands;

import com.calxcymru.minigameapi.MinigameAPI;
import com.calxcymru.minigameapi.arena.Arena;
import com.calxcymru.minigameapi.arena.ArenaManager;
import com.calxcymru.minigameapi.command.Command;
import com.calxcymru.minigameapi.command.CommandManager;
import com.calxcymru.minigameapi.utils.PlayerUtils;
import com.calxcymru.minigameapi.utils.Schematic;
import com.sk89q.worldedit.bukkit.selections.Selection;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Crafted in the heart of Wales!
 *
 * @author CaLxCyMru
 */
public class MinigameCommand {


    public static final String HELP_TEXT = "&cInvalid syntax!\n&b/minigame ";
    public static final String PERMISSION = "&cYou do not have permission to perform this command!";
    public static final String COMMAND = "tw";

    private MinigameAPI api;

    private Player player = null;

    public MinigameCommand(MinigameAPI api) {
        this.api = api;
    }

    @Command(name = "minigame", aliases = {COMMAND, "game", "minigame help", "game help"}, consoleCmd = true, permission = "minigame.help", description = "minigame help")
    public void onHelp(CommandSender sender, String[] args) {
        // Display the help file for the plugin here
        player = (Player) sender;
        api.getCommandManager().showHelp("minigame", sender);
    }

    @Command(name = "minigame create", aliases = {COMMAND + " create", "game create"}, permission = "minigame.arena.create", description = "minigame create <arena>")
     public void onArenaCreate(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.create")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }

        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "create <arena>");
            return;
        }

        Selection arenaSelection = api.getWorldEdit().getSelection(player);
        if (arenaSelection == null || arenaSelection.getMinimumPoint() == null || arenaSelection.getMaximumPoint() == null || api.getWorldEdit().getSession(player) == null) {
            PlayerUtils.sendMessage(player, "&cYou must define where the arena must go with WorldEdit!");
            return;
        }

        if(ArenaManager.getManager().getArena(args[0]) != null) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + args[0] + "&8] &calready exists!");
            return;
        }


        ArenaManager.getManager().createArena(args[0]);

        ArenaManager.getManager().getArena(args[0]).setLocation("min", arenaSelection.getMinimumPoint());
        ArenaManager.getManager().getArena(args[0]).setLocation("max", arenaSelection.getMaximumPoint());

        // Save the arena's schematic
        Schematic.save(player, args[0], api);

        PlayerUtils.sendMessage(player, "&aCreated arena &8[&5" + args[0] + "&8]&a!\n&bBe sure to configure it!");
    }

    @Command(name = "minigame resize", aliases = {COMMAND + " resize", "game resize"}, permission = "minigame.arena.resize", description = "minigame resize <arena>")
    public void onArenaResize(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.resize")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }

        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "resize <arena>");
            return;
        }

        Selection arenaSelection = api.getWorldEdit().getSelection(player);
        if (arenaSelection == null || arenaSelection.getMinimumPoint() == null || arenaSelection.getMaximumPoint() == null || api.getWorldEdit().getSession(player) == null) {
            PlayerUtils.sendMessage(player, "&cYou must define where the arena must go with WorldEdit!");
            return;
        }

        if(ArenaManager.getManager().getArena(args[0]) == null) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + args[0] + "&8] &cdoes not exist!");
            return;
        }

        ArenaManager.getManager().getArena(args[0]).setLocation("min", arenaSelection.getMinimumPoint());
        ArenaManager.getManager().getArena(args[0]).setLocation("max", arenaSelection.getMaximumPoint());

        // Save the arena's schematic
        Schematic.save(player, args[0], api);

        PlayerUtils.sendMessage(player, "&aReszied arena &8[&5" + args[0] + "&8]&a!");
    }

    @Command(name = "minigame remove", aliases = {COMMAND + " remove", "game remove"}, permission = "minigame.arena.remove", description = "minigame remove <arena>")
    public void onArenaRemove(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.remove")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "remove <arena>");
            return;
        }

        if (ArenaManager.getManager().removeArena(args[0])) {
            PlayerUtils.sendMessage(player, "&aRemoved arena &8[&5" + args[0] + "&8]&a!");
        } else {
            PlayerUtils.sendMessage(player, "&cError while removing arena &7" + args[0] + "!\n&cSee console for more details.");
        }
    }

    @Command(name = "minigame setsign", aliases = {COMMAND + " setsign", "game setsign"}, permission = "minigame.arena.setsign", description = "minigame setsign <arena>")
    public void onSignSet(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.setsign")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "setsign <arena>");
            return;
        }
        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!ArenaManager.getManager().getArena(arenaName).isEditable()) {
            PlayerUtils.sendMessage(player, "&cYou cannot edit this arena!");
            return;
        }

        Selection signSelection = api.getWorldEdit().getSelection(player);
        if (signSelection == null || signSelection.getMinimumPoint() == null || signSelection.getMaximumPoint() == null) {
            PlayerUtils.sendMessage(player, "&cYou must select the location where you want the sign to be placed!");
            return;
        }

        if (signSelection.getArea() > 1) {
            PlayerUtils.sendMessage(player, "&cYou must only select one block for the sign!");
            return;
        }

        Block signBlock = signSelection.getMinimumPoint().getBlock();
        if (signBlock == null || !(signBlock.getState() instanceof Sign)) {
            PlayerUtils.sendMessage(player, "&cYou must be looking at a sign!");
            return;
        }
        Arena arena = ArenaManager.getManager().getArena(arenaName);
        arena.setLocation("sign", signBlock.getLocation());
        arena.updateSign();
        PlayerUtils.sendMessage(player, "&aArena sign location has been set to the sign you selected!");
    }

    @Command(name = "minigame reset", aliases = {COMMAND + " reset", "game reset"}, permission = "minigame.arena.reset", description = "minigame reset <arena>")
    public void onArenaReset(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.reset")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }

        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "reset <arena>");
            return;
        }

        if(ArenaManager.getManager().getArena(args[0]) == null) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + args[0] + "&8] &cdoes not exist!");
            return;
        }

        ArenaManager.getManager().getArena(args[0]).removeAll("&c&lARENA IS RESETTING. YOU WERE KICKED.");

        ArenaManager.getManager().getArena(args[0]).resetArena();

        PlayerUtils.sendMessage(player, "&aResetting arena &8[&5" + args[0] + "&8]&a!");
    }


    @Command(name = "minigame setlocation", aliases = {COMMAND + " setlocation", "game setlocation"}, permission = "minigame.arena.setlocation", description = "minigame setlocation <arena> <location name>")
    public void onLocationSet(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.setlocation")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length < 2) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "setlocation <arena> <location name>");
            return;
        }
        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!ArenaManager.getManager().getArena(arenaName).isEditable()) {
            PlayerUtils.sendMessage(player, "&cYou cannot edit this arena!");
            return;
        }

        String option = args[1];

        ArenaManager.getManager().getArena(arenaName).setLocation(option, player.getLocation());
        PlayerUtils.sendMessage(player, "&aSet location &6" + option + "&a to &6where you were standing&a in arena &8[&5" + arenaName + "&8]");
    }

    @Command(name = "minigame settime", aliases = {COMMAND + " settime", "game settime"}, permission = "minigame.arena.settime", description = "minigame settime <arena> <timing name> <time in seconds>")
    public void onTimeSet(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.settime")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length < 2) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "settime <arena> <timing name> <time in seconds>");
            return;
        }
        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!ArenaManager.getManager().getArena(arenaName).isEditable()) {
            PlayerUtils.sendMessage(player, "&cYou cannot edit this arena!");
            return;
        }

        if (!CommandManager.isInt(args[2])) {
            PlayerUtils.sendMessage(player, "&cPlease enter a valid number!");
            return;
        }

        String option = args[1];
        int amount = Integer.parseInt(args[2]);

        ArenaManager.getManager().getArena(arenaName).setTime(option, amount);
        PlayerUtils.sendMessage(player, "&aSet timing for &6" + option + "&a to &6" + amount + "&a in arena &8[&5" + arenaName + "&8]");
    }

    @Command(name = "minigame setplayers", aliases = {COMMAND + " setplayers", "game setplayers"}, permission = "minigame.arena.setplayers", description = "minigame setplayers <arena> <min/max> <amount>")
    public void onPlayersSet(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.setplayers")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length <= 1) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "setplayers <arena> <min/max> <amount>");
            return;
        }
        String arenaName = args[0];

        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!ArenaManager.getManager().getArena(arenaName).isEditable()) {
            PlayerUtils.sendMessage(player, "&cYou cannot edit this arena!");
            return;
        }

        if (!CommandManager.isInt(args[2])) {
            PlayerUtils.sendMessage(player, "&cPlease enter a valid number!");
            return;
        }

        int amount = Integer.parseInt(args[2]);

        String option = args[1];

        if (option.equalsIgnoreCase("min")) {
            ArenaManager.getManager().getArena(arenaName).setMinPlayers(amount);
        } else if (option.equalsIgnoreCase("max")) {
            ArenaManager.getManager().getArena(arenaName).setMaxPlayers(amount);
        } else {
            PlayerUtils.sendMessage(player, "&cYou can &4only &cset the &6MIN &cor &6MAX &aamount of players");
            return;
        }

        PlayerUtils.sendMessage(player, "&aSet &6" + option.toUpperCase() + "&a players to &6" + amount + "&a in arena &8[&5" + arenaName + "&8]");
    }

    @Command(name = "minigame setteam", aliases = {COMMAND + " setteam", "game setteam"}, permission = "minigame.arena.setteam", description = "minigame addteam <arena> <team name> <display name> [prefix]")
    public void onTeamSet(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.setteam")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length < 3) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "setteam <arena> <team name> <display name> [prefix]");
            return;
        }
        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!ArenaManager.getManager().getArena(arenaName).isEditable()) {
            PlayerUtils.sendMessage(player, "&cYou cannot edit this arena!");
            return;
        }

        String team = args[1].toLowerCase();
        String teamName = args[2];
        String teamPrefix = (args.length >= 4) ? args[3] : "";

        ArenaManager.getManager().getArena(arenaName).getTeam(team)
                .setDisplayName(teamName)
                .setPrefix(teamPrefix)
                .setSpawn(player.getLocation());

        PlayerUtils.sendMessage(player, "&aTeam &d" + team + " &aset for arena!\n&6To change the spawn point for the team do: &b/minigame SetSpawn &d" + team);
    }

    @Command(name = "minigame setspawn", aliases = {COMMAND + " setspawn", "game setspawn"}, permission = "minigame.arena.setspawn", description = "minigame setspawn <arena>")
    public void onSpawnSet(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.setspawn")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "setspawn <arena> [team]");
            return;
        }

        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!ArenaManager.getManager().getArena(arenaName).isEditable()) {
            PlayerUtils.sendMessage(player, "&cYou cannot edit this arena!");
            return;
        }
        Arena arena = ArenaManager.getManager().getArena(arenaName);

        String team = (args.length >= 2) ? args[1] : "DEFAULT";

        if (team.equalsIgnoreCase("DEFAULT")) {
            arena.setLocation("defaultSpawn", player.getLocation());
        } else {
            if(team.equalsIgnoreCase("random")) {
                arena.addRandomSpawn(player.getLocation());
                arena.saveRandomSpawns();
            }
            arena.getTeam(team).setSpawn(player.getLocation());
        }

        PlayerUtils.sendMessage(player, "&aThe &b" + team + " &aspawn location has been set to where you were standing!");
    }

    @Command(name = "minigame edit", aliases = {COMMAND + " edit", "game edit"}, permission = "minigame.arena.edit", description = "minigame edit <arena> [true/false]", usage = "test")
    public void onArenaEdit(CommandSender sender, String[] args) {
        player = (Player) sender;

        if (!api.getPermissions().has(player, "minigame.edit")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "edit <arena> [true/false]");
            return;
        }

        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }

        Arena arena = ArenaManager.getManager().getArena(arenaName);

        boolean edit = (args.length >= 2 && CommandManager.isBoolean(args[1])) ? Boolean.parseBoolean(args[1]) : !arena.isEditable();

        arena.setEditable(edit);
        PlayerUtils.sendMessage(player, "&aEditing toggled to &6" + edit + " &afor arena &8[&5" + arenaName + "&8]&a!");
    }

    @Command(name = "minigame join", aliases = {COMMAND + " join", "game join"}, permission = "minigame.join", description = "minigame join <arena>")
    public void onJoin(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.join")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "join <arena>");
            return;
        }
        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!ArenaManager.getManager().getArena(arenaName).isEditable()) {
            PlayerUtils.sendMessage(player, "&cYou cannot edit this arena!");
            return;
        }
        if (!api.getPermissions().has(player, "minigame.join." + arenaName)) {
            return;
        }
        Arena arena = ArenaManager.getManager().getArena(arenaName);
        arena.addPlayer(player);
    }

    @Command(name = "minigame leave", aliases = {COMMAND + " leave", "game leave", "lobby"}, permission = "minigame.leave", description = "minigame leave <arena>")
    public void onLeave(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.leave")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "leave <arena>");
            return;
        }
        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!ArenaManager.getManager().getArena(arenaName).isEditable()) {
            PlayerUtils.sendMessage(player, "&cYou cannot edit this arena!");
            return;
        }
        if (!api.getPermissions().has(player, "minigame.leave." + arenaName)) {
            return;
        }
        Arena arena = ArenaManager.getManager().getArena(arenaName);
        arena.removePlayer(player);
    }

    @Command(name = "minigame start", aliases = {COMMAND + " start", "game start"}, permission = "minigame.start", description = "minigame start <arena>")
    public void onStart(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.start")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "start <arena>");
            return;
        }
        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        // Start an arena
        if (ArenaManager.getManager().getArena(arenaName).getState().getName().equals("lobby")) {
            ArenaManager.getManager().getArena(arenaName).nextState();
        } else {
            PlayerUtils.sendMessage(player, "&cThe game has already started!");
        }

    }

    @Command(name = "minigame stop", aliases = {COMMAND + " stop", "game stop"}, permission = "minigame.stop", description = "minigame stop <arena>")
    public void onStop(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.stop")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length == 0) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "stop <arena>");
            return;
        }
        String arenaName = args[0];
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!(ArenaManager.getManager().getArena(arenaName).getState().getName().equals("lobby"))) {
            ArenaManager.getManager().getArena(arenaName).stopArena();
            PlayerUtils.sendMessage(player, "&aArena has been stopped successfully!");
        } else {
            PlayerUtils.sendMessage(player, "&cArena has not been started yet!");
        }
    }

    @Command(name = "minigame next", aliases = {COMMAND + " next", "game next"}, permission = "minigame.next", description = "minigame next [arena]")
    public void onStateNext(CommandSender sender, String[] args) {
        player = (Player) sender;
        if (!api.getPermissions().has(player, "minigame.next")) {
            PlayerUtils.sendMessage(player, PERMISSION);
            return;
        }
        if (args.length == 0 && Arena.getPlayerArena(player) == null) {
            PlayerUtils.sendMessage(this.player, HELP_TEXT + "next [arena]");
            return;
        }
        String arenaName = (args.length >= 1) ? args[0] : Arena.getPlayerArena(player).getName();
        if (!ArenaManager.getManager().isArena(arenaName)) {
            PlayerUtils.sendMessage(player, "&cArena &8[&5" + arenaName + "&8]&c could not be found!");
            return;
        }
        if (!api.getPermissions().has(player, "minigame.start")) {
            return;
        }


        ArenaManager.getManager().getArena(arenaName).getState().endState(false);
        ArenaManager.getManager().getArena(arenaName).getArenaPlayers().forEach(player -> PlayerUtils.sendMessage(player, "&6Game forced to jump to next game state by &c" + player.getName()));
    }

}
