package com.calxcymru.minigameapi.command;

import com.calxcymru.minigameapi.messages.Message;
import com.calxcymru.minigameapi.utils.PlayerUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manager for the custom command API.
 * This class registers and manages all the commands using the Respawn CommandAPI.
 *
 * @author spaceemotion
 * @version 1.1
 */
public class CommandManager implements CommandExecutor {
    /**
     * The logger we use for errors
     */
    private final Logger log;

    /**
     * The commands stored by their "name"
     */
    private final Map<String, Command> commands;

    /**
     * The methods belonging to the commands
     */
    private final Map<Command, Method> methods;

    /**
     * The objects having the methods
     */
    private final Map<Method, Object> instances;

    /**
     * Creates a new command manager instance.
     */
    public CommandManager(Logger log) {
        this.log = log;

        this.commands = new HashMap<>();
        this.methods = new HashMap<>();
        this.instances = new HashMap<>();
    }

    /**
     * Registers the commands of a certain object.
     *
     * @param object The object holding the annotated methods.
     * @return True on success, false if it failed (check the log files)
     */
    public boolean registerCommands(Object object) {
        boolean success = true;

        for (Method method : object.getClass().getMethods()) {
            if (instances.containsKey(method)) {
                continue;
            }

            try {
                Command cmd = method.getAnnotation(Command.class);
                if (cmd == null) {
                    continue;
                }

                Type returnType = method.getReturnType();
                if (returnType != Boolean.TYPE && returnType != Void.TYPE) {
                    throw new CommandException("Unsupported return type: " + returnType.toString());
                }

                Class<?>[] params = method.getParameterTypes();
                if (params.length != 2 || (params[0] != CommandSender.class) || !(params[1].isArray())) {
                    throw new CommandException("Method " + method.getName() + " does not match the required parameters!");
                }

                commands.put(cmd.name().toLowerCase(), cmd);

                for (String s : cmd.aliases()) {
                    commands.put(s.toLowerCase(), cmd);
                }

                methods.put(cmd, method);
                instances.put(method, object);

            } catch (CommandException e) {
                log.log(Level.WARNING, "Error registering command", e);
                success = false;
            }
        }

        return success;
    }

    @Override
    public boolean onCommand(CommandSender cs, org.bukkit.command.Command command, String label, String[] args) {
        if (!(cs instanceof Player)) {
            PlayerUtils.sendMessage(cs, "&cMinigame related commands can only be run by players!");
            return true;
        }
        // Build arguments
        List<String> arguments = parseArguments(StringUtils.join(args, " "));

        // Search and execute command
        Command cmd;
        String str;

        if (cs == null) {
            cs = Bukkit.getConsoleSender();
        }
        if (label == null) {
            label = "";
        }

        for (int argsLeft = arguments.size(); argsLeft >= 0; argsLeft--) {
            // Build command string
            str = label + ((argsLeft > 0) ? ' ' + StringUtils.join(arguments.subList(0, argsLeft), ' ') : "");

            // Try to get the command - if it can't be found, continue searching
            cmd = getCommand(str);

            if (cmd == null) {
                continue;
            }

            // We have one, execute it!
            try {
                // Check for console / player
                if (!(cs instanceof Player) && (cs instanceof ConsoleCommandSender && !cmd.consoleCmd())) {
                    throw new CommandException("The command cannot be executed from console.");
                }

                // Check permissions
                if (cmd.opOnly() && !cs.isOp() || !hasPermission(cs, cmd.permission())) {
                    throw new CommandException("You are not allowed to use that command!");
                }

                // Check if we want to display the help
                boolean help = args.length > 0 && "?".equalsIgnoreCase(args[args.length - 1]);

                if (!help) {
                    String[] cmdArgs = Arrays.copyOfRange(args, argsLeft, args.length);

                    if (cmdArgs.length < cmd.minArgs() || (cmd.maxArgs() >= 0 && cmdArgs.length < cmd.maxArgs())) {
                        help = true;

                    } else {
                        Method method = methods.get(cmd);

                        if (method == null) {
                            throw new CommandException("Something went wrong: Method not found. Please call an admin!");
                        }

                        if (method.getReturnType() == Boolean.TYPE) {
                            help = !((Boolean) method.invoke(getInstance(method), cs, cmdArgs));

                        } else {
                            method.invoke(getInstance(method), cs, cmdArgs);
                        }
                    }
                }

                if (help) {
                    new Message("&cHelp: &3" + cmd.name()).send(cs);

                    String desc = cmd.description();
                    new Message("&6Description&f: " + (desc.isEmpty() ? "&7(no description available)" : desc)).send(cs);

                    String usage = cmd.usage();
                    new Message("&6Usage: &f" + (usage.isEmpty() ? "/" + cmd.name() : usage)).send(cs);

                    if (cmd.aliases().length > 0) {
                        new Message("&6Aliases: &f/" + StringUtils.join(cmd.aliases(), ", /")).send(cs);
                    }
                }

                return true;

            } catch (CommandException ce) {
                if (!cmd.hide()) {
                    // Send exception message
                    new Message("&4" + ce.getMessage()).send(cs);
                    return true;

                } else {
                    // Return default not found message
                    return false;
                }

            } catch (InvocationTargetException | IllegalAccessException | RuntimeException ex) {
                // Log detailed error
                log.log(Level.SEVERE, "Error executing command", ex);
                // new Message("&6Oh no, an error! Please contact an admin!").send(cs);
                return true;
            }
        }

        // Send error message
        if (args.length > 0) {
            new Message("Command not found: &6" + args[args.length - 1] +
                    " &7(/" + label + ' ' + StringUtils.join(args) + ')').send(cs);

        } else {
            new Message("Command not found: &6" + label).send(cs);
        }

        return true;
    }

    /**
     * Indicates whether or not a command sender has the specified permission.
     *
     * @param sender The sender to check
     * @param perm   The permission to check
     * @return True if he does, false if not
     */
    public boolean hasPermission(CommandSender sender, String perm) {
        if ((!(sender instanceof Player)) || (perm == null) || (perm.isEmpty())) {
            return true;
        }

        Player player = (Player) sender;
        return player.isOp() || player.hasPermission(perm);
    }

    /**
     * Gets a command by its label.
     *
     * @param label The command label
     * @return The found command, or null
     */
    public Command getCommand(String label) {
        return commands.get(label);
    }

    /**
     * Returns a collection of all registered commands.
     *
     * @return All the registered commands in a collection
     */
    public Collection<Command> getCommands() {
        return commands.values();
    }

    /**
     * Sends a detailed help screen to the sender.
     *
     * @param name The name to display in the title
     * @param cs   The command sender that requested the help
     */
    public void showHelp(String name, CommandSender cs) {
        PlayerUtils.sendMessage(cs, "&3" + (name == null ? "" : name + ' ') + "Help");
        PlayerUtils.sendMessage(cs, "&2Add a '?' after each command for a detailed help (like /cmd ?)");

        if (getCommands().isEmpty()) {
            PlayerUtils.sendMessage(cs, "&7(No commands available)");
            return;
        }

        for (Command cmd : getCommands()) {
            if (!hasPermission(cs, cmd.permission()) || !cmd.showInHelp() || (cmd.opOnly() && !cs.isOp())) {
                continue;
            }

            String desc = cmd.description().isEmpty() ? "&7(no description available)" : cmd.description();
            PlayerUtils.sendMessage(cs, "&6/" + cmd.name() + ": &f" + desc);
        }
    }

    List<String> parseArguments(String text) {
        List<String> arguments = new ArrayList<>();
        StringBuilder builder = null;
        boolean quotes = false, reset = true;

        for (int i = 0; i <= text.length(); i++) {
            int c = (i < text.length()) ? text.charAt(i) : -1;

            if (c == -1 || reset) {
                if (builder != null) {
                    String string = builder.toString();

                    if (!string.isEmpty()) {
                        arguments.add(string);
                    }
                }

                builder = new StringBuilder();
                reset = false;

                if (c == -1) {
                    break;
                }
            }

            if (c == '"') {
                if (quotes) {
                    reset = true;
                }

                quotes = !quotes;
                continue;

            } else if (!quotes && c == ' ') {
                reset = true;
                continue;
            }

            builder.append((char) c);
        }

        return arguments;
    }

    private Object getInstance(Method m) {
        return instances.get(m);
    }


    private static class CommandException extends Exception {

        private CommandException(String message) {
            super(message);
        }

    }

    /**
     * Check to see if a string is a valid int
     *
     * @param toCheck String to check
     * @return True: Is valid, False: Not valid
     */
    public static boolean isInt(String toCheck) {
        try {
            Integer.parseInt(toCheck);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check to see if a string is a valid boolean
     *
     * @param toCheck String to check
     * @return True: Is valid, False: Not valid
     */
    public static boolean isBoolean(String toCheck) {
        try {
            Boolean.parseBoolean(toCheck);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
